<?php

return [
    'unknown' => 'غير معرَّف',
    'controls' => 'أدوات التحكم',
    'delete' => 'حذف',
    'add' => 'إضافة',
    'edit' => 'تعديل',
    'reset' => 'إعادة تعيين الحقول',
    'database_exception' => 'حدث خطأ في قاعدة البيانات',
];
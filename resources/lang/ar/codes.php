<?php

return [
    'create_code' => 'إضافة كود جديد',
    'edit_code' => 'تعديل كود',

    'driver_name' => 'اسم السائق',
    'driver_phone_number' => 'رقم موبايل السائق',
    'car_info' => 'بيانات السيارة',
    'car_manufacturer' => 'الشركة المصنعة للسيارة',
    'car_model' => 'موديل السيارة',
    'car_model_year' => 'سنة إصدار الموديل',
    'car_plate_number' => 'رقم السيارة',
    'serial_number' => 'رقم المسلسل',
    'car_code' => 'كود السيارة',
    'no_codes' => 'لا توجد نتائج في الوقت الحالي!',
    'code_not_found' => 'السجل المطلوب غير موجود!',

    'reset_serial_numbers' => 'إعادة تعيين المسلسل',

    'create_success' => 'تم إضافة كود جديد بنجاح',
    'create_fail' => 'فشل إضافة كود جديد',

    'update_success' => 'تم تعديل البيانات بنجاح',
    'update_fail' => 'فشل تعديل البيانات!',

    'delete_success' => 'تم حذف الكود بنجاح',
    'delete_fail' => 'فشل حذف الكود',

    'search_alert_text' => 'عرض النتائج حسب كلمة البحث: <strong>:query</strong> <a href=":link" class="pull-right alert-link">مشاهدة الكل</a>',
];
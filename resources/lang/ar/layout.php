<?php

return [
    'dashboard' => 'لوحة التحكم',
    'notifications' => 'التنبيهات',
    'search' => 'بحث...',
    
    'no_notifications' => 'لا توجد لديك تنبيهات',
    'show_all_notifications' => 'مشاهدة الكل',
];
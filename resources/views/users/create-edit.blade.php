@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="card-box">

                @empty ($editMode)
                <form action="{{ route('users.store') }}" method="post">
                @else
                <form action="{{ route('users.update', $user->getId()) }}" method="post">
                    {!! method_field('PUT') !!}
                @endif
                    {!! csrf_field() !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="form-label">@lang('users.name')</label>
                        <input autofocus required type="text" id="name" name="name" class="form-control" value="{{ old('name', optional(@$user)->getName()) }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="form-label">@lang('users.email')</label>
                        <input required type="email" id="email" name="email" class="form-control" value="{{ old('email', optional(@$user)->getEmail()) }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="form-label">@lang('users.password')</label>
                        <input @empty($editMode) required @endempty type="password" id="password" name="password" class="form-control">
                        @if ($errors->has('password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div><!-- /.form-group -->

                    <input type="hidden" name="user_id" value="{{ optional(@$user)->getId() }}">

                    <button type="submit" class="btn btn-primary" name="submit">
                        @isset($editMode)
                        <i class="fa fa-pencil"></i> @lang('misc.edit')
                        @else
                        <i class="fa fa-plus-circle"></i> @lang('misc.add')
                        @endif
                    </button>
                    <button type="reset" class="btn btn-default">
                        <i class="fa fa-fw fa-refresh"></i> @lang('misc.reset')
                    </button>

                </form>
            </div><!-- /.card-box -->

        </div><!-- /.col -->
    </div><!-- /.row -->

@endsection
@extends('layouts.app', ['layoutOnly' => true])

@section('content')

<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class="text-center">
        <a href="index.html" class="logo"><span>Pears <span>Me</span></span></a>
        <h5 class="text-muted m-t-0 font-600">تفعيل الحساب</h5>
    </div>
    <div class="m-t-40 card-box">
        <div class="panel-body">
            <div class="text-center">{!! $message !!}</div>
        </div>
    </div>
    <!-- end card-box-->
    
</div>
<!-- end wrapper page -->

@endsection
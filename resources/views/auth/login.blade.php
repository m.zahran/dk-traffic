@extends('layouts.app')

@section('content')

<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class="text-center">
        <a href="index.html" class="logo"><span>Dakahlia <span>Traffic</span></span></a>
        <h5 class="text-muted m-t-0 font-600">إدارة مرور الدقهلية</h5>
    </div>
    <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">@lang('auth.welcome')</h4>
        </div>
        <div class="panel-body">
            <form class="form-horizontal m-t-20" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" name="email" required="" placeholder="@lang('users.email')">
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="password" required="" placeholder="@lang('users.password')">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12 col-md-6">
                        <div class="checkbox checkbox-custom">
                            <input id="checkbox-signup" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                            <label for="checkbox-signup">
                                @lang('auth.remember_me')
                            </label>
                        </div>
                    </div>
                    {{--<div class="col-xs-12 col-md-6">
                        <div class="form-group m-t-5">
                            <div class="col-sm-12">
                                <a href="{{ route('password.request') }}" class="text-muted"><i class="fa fa-lock m-r-5"></i> @lang('auth.forgot_password')</a>
                            </div>
                        </div>
                    </div>--}}
                </div>

                <div class="form-group text-center m-t-30 m-b-0">
                    <div class="col-xs-12">
                        <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">@lang('auth.login')</button>
                    </div>
                </div>

            </form>

        </div>
    </div>
    <!-- end card-box-->

    <div dir="ltr" class="clearfix">
        <div class="pull-right">
            <span>Powered by:</span>
            <img src="{{ public_asset('img/logo.png') }}" title="DeltaMedIT" alt="DeltaMedIT" style="height: 32px; margin-top:-10px;" />
        </div>
        <span class="pull-left"><i class="fa fa-fw fa-phone"></i> 01093333696</span>
    </div>

</div>
<!-- end wrapper page -->

@endsection

@extends('layouts.app')

@section('content')

<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class="text-center">
        <a href="index.html" class="logo"><span>Saned <span>Dashboard</span></span></a>
        <h5 class="text-muted m-t-0 font-600">مؤسسة سنِّد أعمالك</h5>
    </div>
    <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">@lang('auth.reset_password')</h4>

            @if (session('status'))
                <p class="text-success m-b-0 font-13 m-t-20">
                    {{ session('status') }}
                </p>
            @endif
        </div>
        <div class="panel-body">
            <form class="form-horizontal m-t-20" role="form" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" type="email" name="email" required="" placeholder="@lang('auth.enter_email')">
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group text-center m-t-20 m-b-0">
                    <div class="col-xs-12">
                        <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">@lang('auth.send_email')</button>
                    </div>
                </div>

            </form>

        </div>
    </div>
    <!-- end card-box-->
    
</div>
<!-- end wrapper page -->

@endsection

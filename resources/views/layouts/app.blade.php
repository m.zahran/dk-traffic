<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Mohamed Zahran 01016084897">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'DK Traffic') }}</title>

    <link rel="shortcut icon" href="{{ public_asset('adminto/images/favicon.ico') }}">

    <!-- Styles -->
    @section('styles')
        <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
        <link href="{{ public_asset('adminto/css/bootstrap-rtl.min.css') }}" rel="stylesheet">
        <link href="{{ public_asset('adminto/css/core.css') }}" rel="stylesheet">
        <link href="{{ public_asset('adminto/css/components.css') }}" rel="stylesheet">
        <link href="{{ public_asset('adminto/css/icons.css') }}" rel="stylesheet">
        <link href="{{ public_asset('adminto/css/pages.css') }}" rel="stylesheet">
        <link href="{{ public_asset('adminto/css/menu.css') }}" rel="stylesheet">
        <link href="{{ public_asset('adminto/css/responsive.css') }}" rel="stylesheet">
        <link href="{{ public_asset('adminto/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
        <link href="{{ public_asset('adminto/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <style>
            body, h1, h2, h3, h4, h5, h6 {
                font-family: 'Cairo', sans-serif;
            }
        </style>

        <script src="{{ public_asset('adminto/js/modernizr.min.js') }}"></script>
    @show
</head>
<body class="fixed-left">

@unless(Auth::guest() or isset($layoutOnly))
    <!-- Begin page -->
    <div id="wrapper">

        @include('layouts.partials.top-bar')

        @include('layouts.partials.left-sidebar')

        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div id="messages"></div>
                    @yield('content')
                </div> <!-- container -->
            </div> <!-- content -->

            <footer class="footer" dir="ltr">
                <div class="clearfix">
                    <div class="pull-right">
                        <span>Powered by:</span>
                        <img src="{{ public_asset('img/logo.png') }}" title="DeltaMedIT" alt="DeltaMedIT" style="height: 32px; margin-top:-10px;" />
                    </div>
                    <span class="pull-left"><i class="fa fa-fw fa-phone"></i> 01093333696</span>
                </div>
            </footer>
        </div>

        @include('layouts.partials.right-sidebar')

    </div>
    <!-- END wrapper -->
@else
    @yield('content')
@endunless

<!-- Scripts -->
<script src="{{ public_asset('adminto/js/jquery.min.js') }}"></script>
<script src="{{ public_asset('adminto/js/bootstrap-rtl.min.js') }}"></script>
@section('scripts')
    <script>
        var resizefunc = [];
    </script>
    <script src="{{ public_asset('adminto/js/detect.js') }}"></script>
    <script src="{{ public_asset('adminto/js/fastclick.js') }}"></script>
    <script src="{{ public_asset('adminto/js/jquery.blockUI.js') }}"></script>
    <script src="{{ public_asset('adminto/js/waves.js') }}"></script>
    <script src="{{ public_asset('adminto/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ public_asset('adminto/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ public_asset('adminto/js/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ public_asset('adminto/plugins/toastr/toastr.min.js') }}"></script>
    <script src="{{ public_asset('adminto/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
    <!-- KNOB JS -->
    <!--[if IE]>
    <script type="text/javascript" src="{{ public_asset('adminto/plugins/jquery-knob/excanvas.js') }}"></script>
    <![endif]-->
    <!-- App js -->
    <script src="{{ public_asset('adminto/js/jquery.core.js') }}"></script>
    <script src="{{ public_asset('adminto/js/jquery.app.js') }}"></script>
    @unless(Auth::guest() or isset($layoutOnly))
        <script>
            (function () {

                @php $authUser = request()->user(); @endphp

                    window.Laravel = <?php echo json_encode([
                    'apiToken' => !empty($authUser) ? $authUser->api_token : null,
                    'baseUrl' => url('/'),
                    'csrfToken' => csrf_token(),
                ]); ?>

                    toastr.options = {
                    "progressBar": true,
                    "positionClass": "toast-bottom-left",
                };

                @if (session('status'))
                    toastr["{{ session('toastr') }}"]("{{ session('status') }}");
                @endif

                $('.delete-btn').on('click', function (e) {
                    e.preventDefault();
                    var el = $(this);
                    swal({
                        title: "هل تريد حقاً حذف العنصر المحدد؟",
                        text: "لن تتمكن من إستعادة العنصر المحذوف مرة أخرى",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "نعم، إحذفه",
                        cancelButtonText: "لا، تراجع",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }, function (isConfirm) {
                        if (isConfirm) {
                            el.closest('form').submit();
                        }
                    });
                });

            })();

        </script>
    @endunless
@show
</body>
</html>
<!-- Top Bar Start -->
<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <a href="{{ route('home') }}" class="logo">
            <span>Dakahlia <span>Traffic</span></span>
            <i class="zmdi zmdi-layers"></i>
        </a>
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">

            <!-- Page title -->
            <ul class="nav navbar-nav navbar-left">
                <li>
                    <button class="button-menu-mobile open-left">
                        <i class="zmdi zmdi-menu"></i>
                    </button>
                </li>
                <li>
                    <h4 class="page-title">{{ $pageTitle or trans('layout.dashboard') }}</h4>
                </li>
            </ul>

            <!-- Right(Notification and Searchbox -->
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden-xs">
                    <form id="search-form" role="search" class="app-search" action="{{ $searchAction or route('codes.index') }}" method="get">
                        <input type="text" placeholder="@lang('layout.search')" class="form-control" name="q" value="{{ request('q') }}">
                    </form>
                </li>
            </ul>

        </div><!-- end container -->
    </div><!-- end navbar -->
</div>
<!-- Top Bar End -->
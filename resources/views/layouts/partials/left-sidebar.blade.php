<!-- Left Sidebar Start -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <!-- User -->
        <div class="user-box">
            <div class="user-img">
                @php
                $email = "{{ request()->user()->email }}";
                $default = "";
                $size = 72;
                $gravatarUrl = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
                @endphp
                <img src="{{ $gravatarUrl }}" alt="{{ request()->user()->name }}" title="{{ request()->user()->name }}" class="img-circle img-thumbnail img-responsive">
            </div>
            <h5><a href="#">{{ str_limit(request()->user()->name, 15) }}</a></h5>
            <ul class="list-inline">
                <li>
                    <a href="{{ route('users.edit', auth()->user()->getAuthIdentifier()) }}" class="text-primary">
                        <i class="zmdi zmdi-account"></i>
                    </a>
                </li>
                <li>
                    <form action="{{ url('logout') }}" method="post">
                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn-link text-danger" style="padding: 0">
                            <i class="zmdi zmdi-power"></i>
                        </button>
                    </form>
                </li>
            </ul>
        </div>
        <!-- End User -->

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <!-- <li class="text-muted menu-title">Navigation</li> -->
                <li><a href="{{ route('home') }}" class="@if (request()->is('home*')) active @endif waves-effect"><i class="zmdi zmdi-view-dashboard"></i><span>@lang('nav.home')</span></a></li>
                <li><a href="{{ route('codes.index') }}" class="@if (request()->is('codes/*')) active @endif waves-effect"><i class="zmdi zmdi-n-5-square"></i><span>@lang('nav.codes')</span></a></li>
                <li><a href="{{ route('users.index') }}" class="@if (request()->is('users/*')) active @endif waves-effect"><i class="zmdi zmdi-account"></i><span>@lang('nav.users')</span></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
</div>
<!-- Left Sidebar End -->
@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="card-box">

                @empty ($editMode)
                <form action="{{ route('codes.store') }}" method="post">
                @else
                <form action="{{ route('codes.update', $code->getId()) }}" method="post">
                    {!! method_field('PUT') !!}
                @endif
                    {!! csrf_field() !!}

                    <div class="form-group{{ $errors->has('serial_number') ? ' has-error' : '' }}">
                        <label for="serial_number" class="form-label">@lang('codes.serial_number')</label>
                        <input autofocus type="number" required="" id="serial_number" name="serial_number" class="form-control" value="{{ old('serial_number', $serialNumber) }}">
                        @if ($errors->has('serial_number'))
                            <span class="help-block">
                            <strong>{{ $errors->first('serial_number') }}</strong>
                        </span>
                        @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('driver_name') ? ' has-error' : '' }}">
                        <label for="driver_name" class="form-label">@lang('codes.driver_name')</label>
                        <input type="text" required="" id="driver_name" name="driver_name" class="form-control" value="{{ old('driver_name', optional(optional(@$code->car)->driver)->getName()) }}">
                        @if ($errors->has('driver_name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('driver_name') }}</strong>
                        </span>
                        @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('car_plate_number') ? ' has-error' : '' }}">
                        <label for="car_plate_number" class="form-label">@lang('codes.car_plate_number')</label>
                        <input required type="text" id="car_plate_number" name="car_plate_number" class="form-control" value="{{ old('car_plate_number', optional(@$code->car)->getPlateNumber()) }}">
                        @if ($errors->has('car_plate_number'))
                            <span class="help-block">
                            <strong>{{ $errors->first('car_plate_number') }}</strong>
                        </span>
                        @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('car_code') ? ' has-error' : '' }}">
                        <label for="car_code" class="form-label">@lang('codes.car_code')</label>
                        <input required type="number" min="0" max="9999999" id="car_code" name="car_code" class="form-control" value="{{ old('car_code', optional(@$code)->getCode()) }}">
                        @if ($errors->has('car_code'))
                            <span class="help-block">
                            <strong>{{ $errors->first('car_code') }}</strong>
                        </span>
                        @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('driver_phone_number') ? ' has-error' : '' }}">
                        <label for="driver_phone_number" class="form-label">@lang('codes.driver_phone_number')</label>
                        <input type="text" id="driver_phone_number" name="driver_phone_number" class="form-control" value="{{ old('driver_phone_number', optional(optional(@$code->car)->driver)->getPhoneNumber()) }}">
                        @if ($errors->has('driver_phone_number'))
                            <span class="help-block">
                            <strong>{{ $errors->first('driver_phone_number') }}</strong>
                        </span>
                        @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('car_manufacturer') ? ' has-error' : '' }}">
                        <label for="car_manufacturer" class="form-label">@lang('codes.car_manufacturer')</label>
                        <input type="text" id="car_manufacturer" name="car_manufacturer" class="form-control" value="{{ old('car_manufacturer', optional(@$code->car)->getManufacturer()) }}">
                        @if ($errors->has('car_manufacturer'))
                            <span class="help-block">
                            <strong>{{ $errors->first('car_manufacturer') }}</strong>
                        </span>
                        @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('car_model') ? ' has-error' : '' }}">
                        <label for="car_model" class="form-label">@lang('codes.car_model')</label>
                        <input type="text" id="car_model" name="car_model" class="form-control" value="{{ old('car_model', optional(@$code->car)->getModel()) }}">
                        @if ($errors->has('car_model'))
                            <span class="help-block">
                            <strong>{{ $errors->first('car_model') }}</strong>
                        </span>
                        @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('car_model_year') ? ' has-error' : '' }}">
                        <label for="car_model_year" class="form-label">@lang('codes.car_model_year')</label>
                        <input type="text" id="car_model_year" name="car_model_year" class="form-control" value="{{ old('car_model_year', optional(@$code->car)->getModelYear()) }}">
                        @if ($errors->has('car_model_year'))
                            <span class="help-block">
                            <strong>{{ $errors->first('car_model_year') }}</strong>
                        </span>
                        @endif
                    </div><!-- /.form-group -->

                    <input type="hidden" name="code_id" value="{{ optional(@$code)->getId() }}">
                    <input type="hidden" name="car_id" value="{{ optional(optional(@$code->car))->getId() }}">

                    <button type="submit" class="btn btn-primary" name="submit">
                        @isset($editMode)
                        <i class="fa fa-pencil"></i> @lang('misc.edit')
                        @else
                        <i class="fa fa-plus-circle"></i> @lang('misc.add')
                        @endif
                    </button>
                    <button type="reset" class="btn btn-default">
                        <i class="fa fa-fw fa-refresh"></i> @lang('misc.reset')
                    </button>

                </form>
            </div><!-- /.card-box -->

        </div><!-- /.col -->
    </div><!-- /.row -->

@endsection
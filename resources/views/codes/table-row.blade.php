<tr>
    <th class="text-center" style="width: 1%" scope="row">{{ $code->getSerialNumber() }}</th>
    <td>{{ $code->car->driver->getName() }}</td>
    <td class="text-center">
        <span class="badge badge-default">{{ $code->car->getPlateNumber() }}</span>
    </td>
    <td class="text-center">
        <span class="badge badge-default">{{ $code->getCode() }}</span>
    </td>
    <td class="text-center">
        {{ $code->car->getManufacturer() ?: trans('misc.unknown') }}
        <span>
            @if ($code->car->getModel())
                {{ $code->car->getModel() }}
            @endif
            @if ($code->car->getModelYear())
                {{ $code->car->getModelYear() }}
            @endif
        </span>
    </td>
    <td class="text-left">
        @include('codes.table-row-controls', ['code' => $code])
    </td>
</tr>
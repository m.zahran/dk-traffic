@extends('layouts.app')

@section('content')

    <div class="m-b-25">
        <a href="{{ route('codes.create') }}" class="btn btn-primary waves-effect">
            <i class="fa fa-fw fa-plus-circle"></i> @lang('codes.create_code')
        </a>
        <a href="{{ route('codes.reset') }}" class="btn btn-inverse waves-effect">
            <i class="fa fa-fw fa-plus-circle"></i> @lang('codes.reset_serial_numbers')
        </a>
    </div>

    @if ($searchQuery)
    <div class="alert alert-info">@lang('codes.search_alert_text', ['query' => $searchQuery, 'link' => route('codes.index')])</div>
    @endif

    <div class="card-box">
        <table class="table table-striped table-responsive m-b-0">
            <thead>
                <tr>
                    <th class="text-center" style="width: 1%">#</th>
                    <th>@lang('codes.driver_name')</th>
                    <th class="text-center">@lang('codes.car_plate_number')</th>
                    <th class="text-center">@lang('codes.car_code')</th>
                    <th class="text-center">@lang('codes.car_info')</th>
                    <th class="text-left" style="width: 25%">@lang('misc.controls')</th>
                </tr>
            </thead>
            <tbody>
                @each('codes.table-row', $codes, 'code', 'codes.table-empty')
            </tbody>
        </table>
    </div>

    <div class="text-center">
        {{ $codes->appends(['q' => $searchQuery])->links() }}
    </div>

@endsection
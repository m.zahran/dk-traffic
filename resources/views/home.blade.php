@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">@lang('home.quick_stats')</h4>
                <div class="row text-center">
                    <div class="col-md-6">
                        <ul class="list-inline">
                            <li>
                                <h3 class="m-b-0">{{ number_format($codesCount) }}</h3>
                                <p class="text-muted">@lang('home.quick_stats_codes')</p>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="list-inline">
                            <li>
                                <h3 class="m-b-0">{{ number_format($usersCount) }}</h3>
                                <p class="text-muted">@lang('home.quick_stats_users')</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

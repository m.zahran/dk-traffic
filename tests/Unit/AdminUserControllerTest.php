<?php

namespace Tests\Unit;

use App\Repositories\UserRepository;
use App\Services\UserService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminUserControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAddUser()
    {
        $userRepository = $this->app->make(UserRepository::class);

        $user = $userRepository->find(1);

        $response = $this->actingAs($user)->get('/users/create');

        $response->assertStatus(200);
    }
}

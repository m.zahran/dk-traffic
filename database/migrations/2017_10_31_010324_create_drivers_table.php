<?php

use App\Driver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Driver::TABLE_NAME, function (Blueprint $table) {
            $table->increments(Driver::FIELD_PK);
            $table->string(Driver::FIELD_NAME);
            $table->string(Driver::FIELD_PHONE_NUMBER)->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Driver::TABLE_NAME);
    }
}

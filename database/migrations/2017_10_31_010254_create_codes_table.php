<?php

use App\Code;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Code::TABLE_NAME, function (Blueprint $table) {
            $table->increments(Code::FIELD_PK);
            $table->unsignedInteger(Code::FIELD_CAR_ID);
            $table->string(Code::FIELD_CODE);
            $table->timestamps();

            $table->index(Code::FIELD_CAR_ID);
            $table->index(Code::FIELD_CODE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Code::TABLE_NAME);
    }
}

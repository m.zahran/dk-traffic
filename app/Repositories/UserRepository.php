<?php

namespace App\Repositories;

use App\User;

class UserRepository extends Repository
{
    /**
     * Sets the name of the eloquent model.
     *
     * @return mixed
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Get an instance of the model.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getModel()
    {
        return $this->model;
    }
}
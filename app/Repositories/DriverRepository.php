<?php

namespace App\Repositories;

use App\Driver;

class DriverRepository extends Repository
{
    /**
     * Sets the name of the eloquent model.
     *
     * @return mixed
     */
    public function model()
    {
        return Driver::class;
    }
}
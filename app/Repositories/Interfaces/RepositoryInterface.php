<?php

namespace App\Repositories\Interfaces;

interface RepositoryInterface
{
    /**
     * Get all the models.
     *
     * @param string|array $columns
     * @return mixed
     */
    public function all($columns = ['*']);

    /**
     * Paginate the models.
     *
     * @param int $perPage
     * @param string|array $columns
     * @return mixed
     */
    public function paginate($perPage = 25, $columns = ['*']);

    /**
     * Create a new model.
     *
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes);

    /**
     * Make a new model instance.
     *
     * @param array $attributes
     * @return mixed
     */
    public function make(array $attributes = []);

    /**
     * Update an existing model.
     *
     * @param array $attributes
     * @param int $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $attributes, $id, $attribute = 'id');

    /**
     * Delete an existing model or destroy a collection of existing models.
     *
     * @param int|array $id
     * @param string $attribute
     * @return mixed
     */
    public function delete($id, $attribute = 'id');

    /**
     * Find a model by Id.
     *
     * @param int $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = ['*']);

    /**
     * Find a model by attribute.
     *
     * @param string $field
     * @param mixed $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($field, $value, $columns = ['*']);

    /**
     * Search for models.
     *
     * @param string $q
     * @param string $column
     * @return mixed
     */
    public function search($q, $column);

    /**
     * Count all models.
     *
     * @return int
     */
    public function countAll();
}
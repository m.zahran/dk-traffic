<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    /**
     * The table schema.
     */

    const TABLE_NAME = 'cars';

    const FIELD_PK = 'id';
    const FIELD_DRIVER_ID = 'driver_id';
    const FIELD_PLATE_NUMBER = 'plate_number';
    const FIELD_MANUFACTURER = 'manufacturer';
    const FIELD_MODEL = 'model';
    const FIELD_MODEL_YEAR = 'model_year';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_DRIVER_ID,
        self::FIELD_PLATE_NUMBER,
        self::FIELD_MANUFACTURER,
        self::FIELD_MODEL,
        self::FIELD_MODEL_YEAR,
    ];

    /*
	|--------------------------------------------------------------------------
	| Getters
	|--------------------------------------------------------------------------
    */

    /**
     * Get the user id.
     *
     * @return int
     */
    public function getId()
    {
        return (int) $this->getAttribute(static::FIELD_PK);
    }

    /**
     * Get the driver id of the car.
     *
     * @return mixed
     */
    public function getDriverId()
    {
        return (int) $this->getAttribute(static::FIELD_DRIVER_ID);
    }

    /**
     * Get the plate number of the car.
     *
     * @return mixed
     */
    public function getPlateNumber()
    {
        return $this->getAttribute(static::FIELD_PLATE_NUMBER);
    }

    /**
     * Get the manufacturer of the car.
     *
     * @return mixed
     */
    public function getManufacturer()
    {
        return $this->getAttribute(static::FIELD_MANUFACTURER);
    }

    /**
     * Get the model of the car.
     *
     * @return mixed
     */
    public function getModel()
    {
        return $this->getAttribute(static::FIELD_MODEL);
    }

    /**
     * Get the model year of the car.
     *
     * @return mixed
     */
    public function getModelYear()
    {
        return $this->getAttribute(static::FIELD_MODEL_YEAR);
    }

    /*
	|--------------------------------------------------------------------------
	| Setters
	|--------------------------------------------------------------------------
    */

    /**
     * Set the driver id of the car.
     *
     * @param int $id
     */
    public function setDriverId(int $id)
    {
        $this->setAttribute(static::FIELD_DRIVER_ID, $id);
    }

    /**
     * Set the plate number of the car.
     *
     * @param string $plateNumber
     */
    public function setPlateNumber(string $plateNumber)
    {
        $this->setAttribute(static::FIELD_PLATE_NUMBER, $plateNumber);
    }

    /**
     * Set the manufacturer of the car.
     *
     * @param string $name
     */
    public function setManufacturer(string $name)
    {
        $this->setAttribute(static::FIELD_MANUFACTURER, $name);
    }

    /**
     * Set the model of the car.
     *
     * @param string $name
     */
    public function setModel(string $name)
    {
        $this->setAttribute(static::FIELD_MODEL, $name);
    }

    /**
     * Set the manufacturer of the car.
     *
     * @param int $year
     */
    public function setModelYear(int $year)
    {
        $this->setAttribute(static::FIELD_MODEL_YEAR, $year);
    }

    /*
	|--------------------------------------------------------------------------
	| Relationships
	|--------------------------------------------------------------------------
    */

    /**
     * The driver that the car belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver()
    {
        return $this->belongsTo(Driver::class)->withDefault([
            Driver::FIELD_NAME => trans('misc.unknown'),
        ]);
    }

    /**
     * The code that belongs to the car.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function code()
    {
        return $this->hasOne(Code::class)->withDefault([
            Code::FIELD_CAR_ID => null,
            Code::FIELD_CODE => null,
        ]);
    }
}

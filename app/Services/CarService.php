<?php

namespace App\Services;

use App\Car;
use App\Repositories\CarRepository;
use App\Http\Requests\CodeRequest;

class CarService
{
    /**
     * @var CarRepository
     */
    private $carRepository;

    /**
     * CarService constructor.
     *
     * @param CarRepository $carRepository
     */
    public function __construct(CarRepository $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    /**
     * Find car by id.
     *
     * @param int $id
     * @return mixed
     */
    public function find(int $id)
    {
        return $this->carRepository->find($id);
    }

    /**
     * Create a new car.
     *
     * @param CodeRequest $request
     * @return mixed
     */
    public function create(CodeRequest $request)
    {
        return $this->carRepository->create($request->only([
            Car::FIELD_DRIVER_ID,
            Car::FIELD_MANUFACTURER,
            Car::FIELD_MODEL,
            Car::FIELD_MODEL_YEAR,
        ]));
    }

    /**
     * Update an existing car.
     *
     * @param CodeRequest $request
     * @param $id
     * @return mixed
     */
    public function update(CodeRequest $request, $id)
    {
        return $this->carRepository->update($request->only([
            Car::FIELD_DRIVER_ID,
            Car::FIELD_MANUFACTURER,
            Car::FIELD_MODEL,
            Car::FIELD_MODEL_YEAR,
        ]), $id);
    }
}
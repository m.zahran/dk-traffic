<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The table schema.
     */

    const TABLE_NAME = 'users';

    const FIELD_PK = 'id';
    const FIELD_NAME = 'name';
    const FIELD_EMAIL = 'email';
    const FIELD_PASSWORD = 'password';
    const FIELD_REMEMBER_TOKEN = 'remember_token';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_EMAIL,
        self::FIELD_PASSWORD,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::FIELD_PASSWORD,
        self::FIELD_REMEMBER_TOKEN,
    ];

    /*
	|--------------------------------------------------------------------------
	| Getters
	|--------------------------------------------------------------------------
    */

    /**
     * Get the user id.
     *
     * @return int
     */
    public function getId()
    {
        return (int) $this->getAttribute(static::FIELD_PK);
    }

    /**
     * Get the name of the user.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->getAttribute(static::FIELD_NAME);
    }

    /**
     * Get the email of the user.
     *
     * @return mixed
     */
    public function getEmail()
    {
        return $this->getAttribute(static::FIELD_EMAIL);
    }

    /**
     * Get the password of the user.
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->getAttribute(static::FIELD_PASSWORD);
    }

    /*
	|--------------------------------------------------------------------------
	| Setters
	|--------------------------------------------------------------------------
    */

    /**
     * Set the name of the user.
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->setAttribute(static::FIELD_NAME, $name);
    }

    /**
     * Set the email of the user.
     *
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->setAttribute(static::FIELD_EMAIL, $email);
    }

    /**
     * Set the password of the user.
     *
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->setAttribute(static::FIELD_PASSWORD, $password);
    }
}

<?php

namespace App\Http\Controllers;

use App\Repositories\CodeRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var CodeRepository
     */
    private $codeRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var array
     */
    private $protectedIds = [1];

    /**
     * Create a new controller instance.
     *
     * @param CodeRepository $codeRepository
     * @param UserRepository $userRepository
     */
    public function __construct(CodeRepository $codeRepository, UserRepository $userRepository)
    {
        $this->codeRepository = $codeRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $codesCount = $this->codeRepository->countAll();
        $usersCount = $this->userRepository->countAll() - count($this->protectedIds);

        return view('home')->with(compact('codesCount', 'usersCount'));
    }
}

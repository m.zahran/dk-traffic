<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Repositories\UserRepository;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * Protected users ids.
     *
     * @var array
     */
    private $protectedIds = [1];

    /**
     * UserController constructor.
     *
     * @param UserService $userService
     * @param UserRepository $userRepository
     */
    public function __construct(UserService $userService, UserRepository $userRepository)
    {
        set_search_action(route('users.index'));

        $this->userService = $userService;
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        set_title(trans('nav.users'));

        $searchQuery = request(KEYWORD_SEARCH_QUERY);

        if ($searchQuery) {
            $users = $this->userService->search($searchQuery);
        } else {
            $users = $this->userService->all(25, $this->protectedIds);
        }

        return view('users.index')->with(compact('users', 'searchQuery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        set_title(trans('users.create_user'));

        return view('users.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        set_title(trans('nav.codes'));

        try {

            if ($this->userService->create(
                request('name'),
                request('email'),
                request('password')
            )) {
                return redirect_success(route('users.index'), trans('users.create_success'));
            }

            return redirect_fail(route('users.create'), trans('users.create_fail'));

        } catch (\Exception $exception) {
            return redirect_fail(route('users.create'), trans('misc.database_exception'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        set_title(trans('users.edit_user'));

        enable_edit_mode();

        try {

            // Throw an exception if the user is not editable.
            if ( ! $this->userEditable($id)) {
                throw new ModelNotFoundException();
            }

            $user = $this->userRepository->find($id);

            return view('users.create-edit')->with(compact('user'));

        } catch (ModelNotFoundException $exception) {
            return redirect_fail(route('users.index'), trans('users.user_not_found'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        try {

            // Throw an exception if the user is not editable.
            if ( ! $this->userEditable($id)) {
                throw new ModelNotFoundException();
            }

            if ($this->userService->update(
                $id,
                request('name'),
                request('email'),
                request('password')
            )) {
                return redirect_success(route('users.index'), trans('users.update_success'));
            }

            return redirect_fail(route('users.update'), trans('users.update_fail'));

        } catch (\Exception $exception) {
            return redirect_fail(route('users.update'), trans('misc.database_exception'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            // Let's make sure that the user don't delete himself/herself.
            array_push($this->protectedIds, auth()->user()->getAuthIdentifier());

            // Throw an exception if the user is not editable.
            if ( ! $this->userEditable($id)) {
                throw new ModelNotFoundException();
            }

            if ($this->userService->delete($id)) {
                return redirect_success(route('users.index'), trans('users.delete_success'));
            }

            return redirect_fail(route('users.index'), trans('users.delete_fail'));

        } catch (\Exception $exception) {
            return redirect_fail(route('users.index'), trans('misc.database_exception'));
        }
    }

    /**
     * Check if the user is editable.
     *
     * @param $id
     * @return bool
     */
    private function userEditable($id)
    {
        return ! in_array($id, $this->protectedIds);
    }
}

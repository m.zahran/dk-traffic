<?php

namespace App\Http\Controllers;

use App\Cache\CodeCache;
use App\Http\Requests\CodeRequest;
use App\Repositories\CodeRepository;
use App\Services\CodeService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CodeController extends Controller
{
    /**
     * @var CodeRepository
     */
    private $codeRepository;

    /**
     * @var CodeService
     */
    private $codeService;

    /**
     * @var CodeCache
     */
    private $codeCache;

    /**
     * CodeController constructor.
     *
     * @param CodeService $codeService
     * @param CodeRepository $codeRepository
     */
    public function __construct(CodeService $codeService, CodeRepository $codeRepository)
    {
        set_search_action(route('codes.index'));

        $this->codeService = $codeService;
        $this->codeRepository = $codeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        set_title(trans('nav.codes'));

        $searchQuery = request(KEYWORD_SEARCH_QUERY);

        if ($searchQuery) {
            $codes = $this->codeService->search($searchQuery);
        } else {
            $codes = $this->codeService->all();
        }

        return view('codes.index')->with(compact('codes', 'searchQuery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        set_title(trans('codes.create_code'));

        $serialNumber = $this->codeService->countAll() + 1;

        return view('codes.create-edit', compact('serialNumber'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CodeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CodeRequest $request)
    {
        try {

            if ($this->codeService->create(
                request('driver_name'),
                request('driver_phone_number'),
                request('car_plate_number'),
                request('car_manufacturer'),
                request('car_model'),
                request('car_model_year'),
                request('serial_number'),
                request('car_code')
            )) {
                return redirect_success(route('codes.index'), trans('codes.create_success'));
            }

            return redirect_fail(route('codes.create'), trans('codes.create_fail'));

        } catch (\Exception $exception) {
            return redirect_fail(route('codes.create'), trans('misc.database_exception'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        set_title(trans('codes.edit_code'));

        enable_edit_mode();

        try {

            $this->codeRepository->setRelations([
                'car.driver',
            ]);

            $code = $this->codeRepository->find($id);

            $serialNumber = optional(@$code)->getSerialNumber();

            return view('codes.create-edit')->with(compact('code', 'serialNumber'));

        } catch (ModelNotFoundException $exception) {
            return redirect_fail(route('codes.create'), trans('codes.code_not_found'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CodeRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CodeRequest $request, $id)
    {
        try {

            if ($this->codeService->update(
                $id,
                request('driver_name'),
                request('driver_phone_number'),
                request('car_plate_number'),
                request('car_manufacturer'),
                request('car_model'),
                request('car_model_year'),
                request('serial_number'),
                request('car_code')
            )) {
                return redirect_success(route('codes.index'), trans('codes.update_success'));
            }

            return redirect_fail(route('codes.update', $id), trans('codes.update_fail'));

        } catch (\Exception $exception) {
            return redirect_fail(route('codes.update', $id), trans('misc.database_exception'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            if ($this->codeService->delete($id)) {
                return redirect_success(route('codes.index'), trans('codes.delete_success'));
            }

            return redirect_fail(route('codes.index'), trans('codes.delete_fail'));

        } catch (\Exception $exception) {
            return redirect_fail(route('codes.index'), trans('misc.database_exception'));
        }
    }
}

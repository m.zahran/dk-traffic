<?php

namespace App\Http\Requests;

use App\Car;
use App\Code;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'driver_name' => 'required',
            'driver_phone_number' => '',
            'car_manufacturer' => '',
            'car_model' => '',
            'car_model_year' => '',
        ];

        $rules['car_code'] = 'required|unique:' . Code::TABLE_NAME . ',' . Code::FIELD_CODE . ',' . request('code_id');
        $rules['car_plate_number'] = 'required|unique:' . Car::TABLE_NAME . ',' . Car::FIELD_PLATE_NUMBER . ',' . request('car_id');

        return $rules;
    }
}
